<?php
  require_once("path/to/cockpit/bootstrap.php");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Starterkit by Andreas Gruber</title>

    <!-- Bootstrap -->
    <link href="public/third-party/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animate.css -->
    <link href="public/third-party/animate.css/animate.min.css" rel="stylesheet">

    <!-- Application Styles -->
    <link href="public/css/app.css" rel="stylesheet">

    <!-- Modernizr -->
    <script src="public/third-party/modernizr/modernizr.js"></script>

    <!-- Detectizr -->
    <script src="public/third-party/detectizr/dist/detectizr.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="public/third-party/html5shiv/dist/html5shiv.js"></script>
      <script src="public/third-party/respond/dest/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

    <!-- Your HTML code goes here -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Brand</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          <li class="active"><a href="#">Link</a></li>
          <li><a href="#">Link</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
              <li class="divider"></li>
              <li><a href="#">One more separated link</a></li>
            </ul>
          </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container">
      <div class="row">
          <div class="col-md-12 text-center">
            <img src="public/img/test.png">
            <h1>Starterkit</h1>
            <p>by <a href="mailto:andreas@webbackstube.at">Andreas Gruber</a></p>
          </div>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="public/third-party/jquery/dist/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="public/third-party/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Application Script -->
    <script src="public/js/app.js"></script>

  </body>
</html>